var Pages = new Array(
    'areas/buttons.html',
    'areas/content.html',
    'areas/forms.html',
    'areas/lists.html'
);

var Sides = new Array(
    'areas/mainmenu.html',
    'areas/sidemenu.html'
);

var CSS = new Array(
    'blank.html'
);

var JS = new Array(
    'blank.html'
);

var Templates1 = new Array(
    'blank.html',
    'blank.html'
);

function yellaOnLoadFinish(){
    console.log("ended");
}

Yella.init(Pages, Sides, CSS, JS, Templates1, yellaOnLoadFinish);

/*
 * This is used to prevent the main viewport from moving. We assign this to the <body> tag
 */
bodytouchMove = function(event) {
    event.preventDefault();
}