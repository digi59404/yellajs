var Yella =(function($) {
	var LIB = {
	
		"IconColor" : 'black',
		"INITATED": 'No',
		/**
		* Init function:
		* This function is meant to load all the files listen into the dom.
		* It gets them ready to be switched and loaded too.
		*
		* This function also sets up the hash handlers for when a hash is clicked.
		*
		* Example:
		* var array = new array('page1', 'page2');
		* Yella.init(array);
		**/
		"init" : function(pages, sides, css, js, templates, finFunc){
			if(LIB.INITIATED === 'YES'){
				return;
			}
			var len = pages.length;
			var alen = sides.length;
			var jlen = js.length;
			var csslen = css.length;
			var tlens = templates.length;
			
			var body_data = '';
			if(len > 0){
				
				for(var i=0; i<len; i++){
					var value = pages[i];
					var page = $.ajax({
						dataType: 'text',
						type: 'get',
						url: pages[i],
						async: false,
						success: function(data){
							body_data = body_data+data;
						},
						error: function(data){
							console.log('ERROR LOADING: '+pages[i]);
						}
					});
				}
				$('#sections').append(body_data);
			}
			var body_data = '';
			if(alen > 0){
				
				for(var i=0; i<alen; i++){
					var value = sides[i];
					var side = $.ajax({
						dataType: 'text',
						type: 'get',
						url: sides[i],
						async: false,
						success: function(data){
							body_data = data
							$('body').append(body_data);
							
						},
						error: function(data){
							console.log('ERROR LOADING: '+pages[i]);
						}
					});
				}
				
			}
			var template_data = '';
			if(tlens > 0) {
				
				for(var b=0; b<tlens; b++){
					
					//var value = templates[i];
					var tem = $.ajax({
						dataType: 'text',
						type: 'GET',
						url: templates[b],
						async: false,
						success: function(data){
							template_data = template_data+data;
						},
						error: function(data){
							console.log('ERROR LOADING: '+templates[b]);
						}
					});
				}
				$('div#Templates').append(template_data);
			}
			if(csslen > 0){
				for(var i=0; i<csslen; i++){
					$("head").append("<link>");
				    css1 = $("head").children(":last");
				    css1.attr({
				      rel:  "stylesheet",
				      type: "text/css",
				      href: css[i]
				    });
				}
			}
			if(jlen > 0){
				for(var i=0; i<jlen; i++){
					var value = js[i];
					LIB.loadJS(value, function(data, textStatus){
						//console.log(value+': '+textStatus);
					});
				}
			}
			finFunc();
			LIB.INITIATED = "YES";
			
			
			$('a[data-target=aside]').bind('click', function(){
				var id = $(this).attr('data-aside').substr(1);
				var side = $(this).attr('data-apos');
				LIB.doAside(id, side);
				
			});
			$(window).bind('hashchange', function() {
				var h = window.location.hash.split('/');
				h[0] = h[0].substr(1);
				if(h[0].length < 1){
					//hash = "main";
				} else {
					if(!h[1]){
						LIB.switchPage(h[0]);
					} else {
						LIB.switchArticle(h[0], h[1], 1);
					}
				}
			});
			
		},
		
		
		/**
		* Switch function:
		* This function handles the routing. It disables the main page and 
		* switches to a new page. It also loads the first article in a page.
		*
		* Example:
		* Yella.switchPage('NewPageID');
		**/
		"switchPage": function(pid){
			//$('section.active').slideDown('slow', function(){
			if(pid.length < 1){
				pid = "main";
			}
			var page = '#'+pid;
			var article = '#'+pid+' article#main';
			

			
			$('section.active').slideToggle("slow", function() {
				$('section').removeClass('aside-l');
				$('section').removeClass('aside-r');
				//$('section').removeClass('aside');
				
				$(page).addClass('active');
				$(article).addClass('active');
				
				$(page).slideToggle("slow", function() {
					$(page).stop();
				});
				 
				$('aside').removeClass('active');
				$('section').removeClass('active');
				
				$('section.active').removeClass('active');
				$('section article.active').removeClass('active');
			  	$('section.active').stop();
			  	
			  	window.location.hash = '#'+pid+'/main';
			  	
			});
			$('#loading-bar').hide();
			
		},
		
		/**
		* Switch function:
		* This function handles the routing. It disables the main page and 
		* switches to a new page. It also loads the first article in a page.
		*
		* Example:
		* Yella.switchArticle('NewPageID');
		**/
		"switchArticle": function(pid, aid, event){
			var page = '#'+pid;
			var article = '#'+pid+' article#'+aid;
			if(event == '1'){
				$('section').removeClass('aside');
				$('aside').removeClass('active');
				$('section.active').removeClass('active');
				$('section article.active').removeClass('active');
				$('section').removeClass('aside-l');
				$('section').removeClass('aside-r');
				
				$(page).addClass('active');
				$(article).addClass('active');
				
			} else {
					
					$('section.active').addClass('old');
					$('section.active').css('z-index', '4 !important');
					$('section article.active').addClass('old');
					$(page).addClass('active');
					$(article).addClass('active');
					$(page).slideDown("slow", function() {
						
						
							
					$('section').removeClass('aside-l');
					$('section').removeClass('aside-r');

					$('section.old').removeClass('active');
					$('section article.old').removeClass('active');
					$('section.old').removeClass('old');
					$('section article.old').removeClass('old');
					window.location.hash = '#'+pid+'/'+aid;
				});
				//
			}
			if(typeof eval($(article).data("pageload")) == "function"){
				eval($(article).data("pageload"))
			}
			$('#loading-bar').hide();
		},
		
		"doAside": function(aid, side){
			if(side == "right"){
				aside = "aside-r";
			} else {
				aside = "aside-l";
			}
			if($('section.active').hasClass('aside-l') || $('section').hasClass('aside-r')){
				$('section').removeClass('aside-l');
				$('section').removeClass('aside-r');
				$('#'+aid).removeClass('active');
			}else {
				$('section').addClass(aside);
				$('#'+aid).addClass('active');
			}
		},
		
		"loadJS": function loadJs(src, callback) {
		    var s = document.createElement('script');
		    document.getElementsByTagName('head')[0].appendChild(s);
		    s.onload = function() {
		        //callback if existent.
		        if (typeof callback == "function") callback();
		        callback = null;
		    }
		    s.onreadystatechange = function() {
		        if (s.readyState == 4 || s.readyState == "complete") {
		            if (typeof callback == "function") callback();
		            callback = null; // Wipe callback, to prevent multiple calls.
		        }
		    }
		    s.src = src;
		},
		
		/**
		* Icon Switch function:
		* This function changes span elements into icons. Any span with a class of icon, 
		* and data-type of 'data-icon=""' will be changed into an icon.
		*
		* Example:
		* NO EXAMPLE - Should be Used Internally.
		**/
		
		"doIcons": function(){
			//$('a').attr("data-icon").each(function(index){
			//	alert(this);
			//});
		}
	};
	return LIB;
})(jQuery);


Yella.DB = (function($, LIB) {
	var DB = {
		"openDB": function(sname, version, displayName){
			if(!window.openDatabase){
				alert("No Database Support... Closing");
			}else {
				DB.DataBase = openDatabase(sname, version, displayName, 100000);
			}
		},
		"doSQL": function(sql, data, cb, ecb){
			DB.DataBase.transaction(
				function(transaction){
					transaction.executeSql(sql, data, cb, ecb);
				}
			);
		}
	};
	return DB;
})(jQuery, Yella);

